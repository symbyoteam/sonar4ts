#
# Creates a docker container with SonarQube
#

FROM sonarqube:6.3.1

MAINTAINER Mahmoud Samy <mahmoud.samy@symbyo.com>

ENV SONAR_VERSION=6.3.1 \
    SONARQUBE_HOME=/opt/sonarqube \
    # Database configuration
    # Defaults to using H2
    SONARQUBE_JDBC_USERNAME=sonar \
    SONARQUBE_JDBC_PASSWORD=sonar \
    SONARQUBE_JDBC_URL=

# Http port
EXPOSE 9000

RUN set -x \
    && cd ~ \
    && curl -sL https://deb.nodesource.com/setup_6.x -o nodesource_setup.sh \
    && apt-get update \
    && bash nodesource_setup.sh \
    && apt-get install nodejs \
    && npm install -g tslint typescript \
    && cd $SONARQUBE_HOME/extensions/plugins \
    && wget https://github.com/Pablissimo/SonarTsPlugin/releases/download/v1.1.0/sonar-typescript-plugin-1.1.0.jar

VOLUME "$SONARQUBE_HOME/data"

WORKDIR $SONARQUBE_HOME
COPY run.sh $SONARQUBE_HOME/bin/
ENTRYPOINT ["./bin/run.sh"]